import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
/**
 * performs queries on the database
 * @author Vahid Nasri Nasrabadi, Masoud Sadeghi, AmirFoad Sam
 *
 */
public class SQLQueries {
	ArrayList<Lead> leads = new ArrayList<>(); 
	SQLConnection newConnection = new SQLConnection();
	private Connection con = newConnection.getConn();
	/**
	 * shows the entire DB 
	 * @throws SQLException
	 */
	public void showDataBase() throws SQLException {
		Statement mystatement = con.createStatement();
		ResultSet myRs = mystatement.executeQuery("select * from leads");
		while(myRs.next()) {
			System.out.println(myRs.getString("FirstName") + " " + myRs.getString("LastName") + " " + myRs.getString("ContactInfo"));
		}
	}
	/**
	 *  adds a record to DB using the parameters
	 * @param firstname given by the user
	 * @param lastname given by the user
	 * @param contactinfo given by the user
	 * @throws SQLException given by the user
	 */
	public void addToDatabase(String firstname,String lastname,String contactinfo) throws SQLException {
		leads.add(new Lead(firstname,lastname,contactinfo));
		String sql = "insert into leads ( FirstName, LastName, ContactInfo)" + " values (?, ?, ?)";
		PreparedStatement preparedStmt = con.prepareStatement(sql);
		preparedStmt.setString (1, leads.get(leads.size()-1).getName());
		preparedStmt.setString (2, leads.get(leads.size()-1).getLastname());
		preparedStmt.setString(3, leads.get(leads.size()-1).getContactinfo());
		preparedStmt.execute();
	}
	/**
	 * search the database for the fields based on parameters 
	 * @param Firstname given by the user
	 * @param Lastname given by the user
	 * @return a boolean if the record is found in the DB.
	 * @throws SQLException
	 */
	public boolean searchInDatabase(String Firstname,String Lastname) throws SQLException {
		Statement mystatement = con.createStatement();
		String sql = "select * from leads WHERE Firstname='" + Firstname +"'"+ " AND LastName='"+Lastname+"'";
		ResultSet myRs = mystatement.executeQuery(sql);
		while(myRs.next()) {
			System.out.println(myRs.getString("FirstName") + " " + myRs.getString("LastName") + " " + myRs.getString("ContactInfo"));
		}
		return true;
	}
}

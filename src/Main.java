import java.sql.*;
import java.util.Scanner;
/**
 * Main class that is responsible for interacting with user through command line.
 * @author Vahid Nasri Nasrabadi, Masoud Sadeghi, AmirFoad Sam
 *
 */
public class Main {
	private static Scanner scanner = new Scanner(System.in);
	private static SQLQueries sqlq = new SQLQueries();
	public static void main(String[] args) throws SQLException {

		// TODO Auto-generated method stub
		boolean quit = false; 
		while(!quit) {
			printActions(); 
			int action = 0;
			if(scanner.hasNextInt())

				action = scanner.nextInt();

			scanner.nextLine();
			// switch case for user interaction
			switch (action) { 

			case 1:
				sqlq.showDataBase();
				// show all the items that are currently inside database
				break;
			case 2:
				addToDataBase();
				// add a record to database
				break;
			case 3:
				searchInDataBase();
				// search the database based on the given record from the user and show it 
				break;
			case 9:
				quit = true;   // to quit the program
				break;
			default:
				System.out.println("Wrong input, Please Enter one of the numbers listed below");
				printActions();

			}
		}			
	}
	/**
	 * print all the actions that user can choose from 
	 */
	private static void printActions() {
		System.out.println("\n Actions:\n Connection to DataBase Established \n Press");
		System.out.println("1  - to display all leads in the database \n" +
				"2  - to add a lead to the database\n" +
				"3  - search for leads.\n"+
				"9  - quit\n");
		System.out.println("Choose your action: ");
	}
	/**
	 * asks for the informations required to add a record to DB and calls the query function
	 * @throws SQLException
	 */
	private static void addToDataBase() throws SQLException {
		System.out.println("Action: \n Enter 'FirstName' \n");
		String firstname = scanner.nextLine();
		System.out.println("Action: \n Enter 'LastName' \n");
		String lastname = scanner.nextLine();
		System.out.println("Action: \n Enter 'ContactInfo' \n");
		String contactinfo = scanner.nextLine();
		sqlq.addToDatabase(firstname, lastname, contactinfo);
	}
	/**
	 * asks for First name and Last name and calls the DB query to show the record
	 */
	private static void searchInDataBase() {
		System.out.println("Action: \n Enter First Name of the lead ");
		String Fristname = scanner.nextLine();
		System.out.println("Action: \n Enter Last Name of the lead ");
		String Lastname = scanner.nextLine();
		try {
			sqlq.searchInDatabase(Fristname,Lastname);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

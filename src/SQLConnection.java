import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * 
 * @author Vahid Nasri Nasrabadi, Masoud Sadeghi, AmirFoad Sam
 * Establish a connection to database for further use.
 */
public class SQLConnection {
	private  String username = "sql3320485";
	private  String password = "aDNNK67CZI";
	private  String dbURL = "jdbc:mysql://sql3.freemysqlhosting.net:3306/sql3320485";
	private Connection conn = null;
	public SQLConnection() {
		try {
			this.connectToDataBase();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * connect to database with the username, password and url provided
	 * @throws SQLException
	 */
	private void connectToDataBase() throws SQLException {
		try {
			System.out.println("connection established");
			conn = DriverManager.getConnection(dbURL, username, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * getter for conn variable
	 * @return
	 */
	public Connection getConn() {
		return conn;
	}
}
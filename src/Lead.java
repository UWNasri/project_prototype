
public class Lead {
	private String name;
	private String lastname;
	private String contactinfo;
	public Lead(String name, String lastname, String contactinfo2) {
		super();
		this.name = name;
		this.lastname = lastname;
		this.contactinfo = contactinfo2;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getContactinfo() {
		return contactinfo;
	}
	public void setContactinfo(String contactinfo) {
		this.contactinfo = contactinfo;
	}
	
}
